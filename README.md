**MEGA.CZ CHAT HTML DOM Parser**

This is very simple PHP script, that parses localy saved HTML DOM of MEGA.CZ chat.

---

## Insturctions



1. Simply go to chat and save it from browser with (CTRL+S), rename file to index.html
2. Download and install XAMPP webserver or something similar (https://www.apachefriends.org/index.html)
3. Start XAMPP control panel and run webserver. Put contents of directory in webservers "htdocs" directory.
4. After go to http://localhost/example and You will see demo results.
5. After upload saved chat HTML to example/html directory overwriting the existing one.
6. After refresh web page and You will see chatlogs. Save them as HTML.

## Roadmap

1. Automatic media download and categorization by username.

## Suggestions
`If You have some suggestions, please contact me (how? it's up to You). :)
