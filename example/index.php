<style>
    th, td {
        border-bottom: 1px solid #ddd;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2;
    }
</style>
<?php
/**
 * Web server configuration stuff
 */
//error_reporting(E_ALL);
error_reporting(0);
ini_set('max_execution_time', 0);
ini_set('memory_limit', '512M');

/**
 * Actual code
 */

include('../simple_html_dom.php');

// Create DOM from URL
$html = file_get_html('html/index.html');
$parseData = $html->find('[class*=message body]');


// Find all chatLog blocks
foreach ($parseData as $chatLog) {
    $item['username'] = $chatLog->find('div.user-card-name', 0)->plaintext;

    //If the message is sent by same user. Insert reference about it.

    //$item['dateTime']    = $chatLog->find('div.simpletip', 0)->attr;

    if (is_array($chatLog->find('div.simpletip', 0)->attr)) {
        $item['dateTime'] = $chatLog->find('div.simpletip', 0)->attr;
        $item['dateTime'] = $item['dateTime']["data-simpletip"];
    }

    $item['chatMessage'] = $chatLog->find('div.text-block', 0)->plaintext;
    //If message hasn't content, check if instead user has send media file and get media name.
    if (is_null($item['chatMessage'])) {
        $preText = $chatLog->find('div.data-title', 0)->plaintext;
        $fileSize = $chatLog->find('div.file-size', 0)->plaintext;
        $imageLink = $chatLog->find('img.thumbnail-placeholder', 0)->src;
        if (is_null($imageLink)) {
            continue;
        }

        //todo: If image has type: blob it won't download.
        /*
        if(!is_null($imageLink)){
            $str = substr($imageLink, 5);
            echo $str;
            die();
            $content = file_get_contents($str);
            file_put_contents('./'.$fileName.'', $content);
        }
        */

        $item['chatMessage'] = $preText . ' -  ' . $fileSize . ' - <a href="' . $imageLink . '">Download image</a>';
    }
    $chatLogs[] = $item;
}
?>
<table style="width:100%; border: 2px solid black;">
    <tr>
        <th>Username</th>
        <th>Date time</th>
        <th>Chat message</th>
    </tr>
    <?php
    foreach ($chatLogs as $chatLogData):
        ?>
        <tr>
            <td width="150"><?= $chatLogData['username']; ?></td>
            <td width="100" align="center"><?= $chatLogData['dateTime']; ?></td>
            <td><?= $chatLogData['chatMessage']; ?></td>
        </tr>
    <?php endforeach; ?>
</table> 